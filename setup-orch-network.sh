#!/bin/bash

VLANEXISTS=$(ip a | grep -c "eno1.40" 2> /dev/null)
if [ $VLANEXISTS = 0 ]; then
    LANIF=eno1
    VLANIF=${LANIF}.40
    VLANMAC=44:99:33:66:11:13
    VLANIP=172.168.10.13
    EPCIP=172.168.10.10

    sudo ip link add link $LANIF name $VLANIF address $VLANMAC type vlan id 40
    sudo ip link set dev $VLANIF up
    sudo ip addr add ${VLANIP}/24 dev $VLANIF

    sudo ip route add 12.0.0.0/24 via $EPCIP dev $VLANIF
    sudo ip route add 12.1.1.0/24 via $EPCIP dev $VLANIF
fi
