#!/usr/bin/env python

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN


tourDescription = """

# TDD Test Profile

Use this profile for TDD development and testing in a controlled RF environment
(SDRs with wired connections). It instantiates and end-to-end LTE network using
OAI (RAN and CN) and srsLTE (UE), as well as an orchestration node for testing
network throughput. It is largely based on
[this](https://github.com/OPENAIRINTERFACE/openair-cn/wiki/Full-Deployment-of-OAI-system)
tutorial.

The following nodes will be deployed:

* Intel nuc8650/B210 w/ srsLTE (`rue1`)
* Supermicro e300-8d/B210 w/ OAI RAN (`enb1`)
* Intel nuc8650 w/ OAI CN (`epc`)
* Supermicro e300-8d for orchestrating tests (`orch`)

"""

tourInstructions = """

### Preliminary setup after the experiment becomes ready

After the nodes boot for the first time, ssh into `rue1`, `enb`, and `epc` and
do the following:

```
cd /local/repository
./setup.sh
```

This will install the necessary software and configuration files on each node.
It may take several minutes for the `enb1` and `epc` nodes. The script will exit
on error; make sure it completes on all three nodes before moving on. These
changes will persist after rebooting the nodes, so you only need to do this the
first time the nodes boot.

### Starting the end-to-end network

After the preliminary setup has completed without error, do the following, first
on `epc`, next on `enb`, and finally on `rue1` (the order is important here):

```
cd /local/repository
./start.sh
```

Note that, on `epc`, this will start a ```tmux``` session with four panes,
running HSS, MME, SPGW-C, and SPGW-U. If you are not familiar with ```tmux```,
it's a terminal multiplexer that has some similarities to screen. Here's a [tmux
cheat sheet](https://tmuxcheatsheet.com), but ```ctrl-b o``` (move to other
pane) and ```ctrl-b x``` (kill pane), should get you pretty far. If you'd rather
start the `epc` processes another way, here are the commands (order is important
here as well):

```
# in separate consoles, unless you're backrgounding the processes

# start HSS
cd /local/openair-cn/scripts
oai_hss -j /usr/local/etc/oai/hss_rel14.json

# start MME
cd /local/openair-cn/scripts
./run_mme --config-file /usr/local/etc/oai/mme.conf --set-virt-if

# start SPGW-C
sudo spgwc -c /usr/local/etc/oai/spgw_c.conf

# start SPGW-U
sudo spgwu -c /usr/local/etc/oai/spgw_u.conf
```

### Testing

You can use the orchestration node ```orch``` to test the network.

#### Ping the UE
on ```orch```

```
ping -c 20 12.1.1.2
```
#### Iperf DL UDP
on ```rue1```

```
iperf -B 12.1.1.2 -u -s -i 1 -fm
```

on ```orch```

```
iperf -c 12.1.1.2 -u -b 2.00M -t 30 -i 1 -fm
```

#### Iperf UL UDP
on ```orch```

```
iperf -u -s -i 1 -p 5001 -fm
```

on ```rue1```

```
iperf -c 172.168.10.13 -u -b 2.00M -t 30 -i 1 -fm  -p 5001 -B 12.1.1.2
```
"""


class GLOBALS(object):
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    UBUNTU_OAI_EPC_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    UBUNTU_OAI_RAN_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:ubuntu1804lowlatency:1"


pc = portal.Context()
request = pc.makeRequestRSpec()

enb1 = request.RawPC("enb1")
enb1.component_manager_id = "urn:publicid:IDN+test1.powderwireless.net+authority+cm"
enb1.component_id = "ed2"
enb1.disk_image = GLOBALS.UBUNTU_OAI_RAN_IMG
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))

rue1 = request.RawPC("rue1")
rue1.component_manager_id = "urn:publicid:IDN+test1.powderwireless.net+authority+cm"
rue1.component_id = "nuc2"
rue1.disk_image = GLOBALS.UBUNTU_OAI_RAN_IMG
rue1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))

epc = request.RawPC("epc")
epc.component_manager_id = "urn:publicid:IDN+test1.powderwireless.net+authority+cm"
epc.component_id = "nuc1"
epc.disk_image = GLOBALS.UBUNTU_OAI_EPC_IMG
# epc.addService(rspec.Execute(shell="bash", command="/local/repository/setup-epc-network.sh"))

orch = request.RawPC("orch")
orch.component_manager_id = "urn:publicid:IDN+test1.powderwireless.net+authority+cm"
orch.component_id = "ed3"
orch.disk_image = GLOBALS.UBUNTU_1804_IMG
orch.addService(rspec.Execute(shell="bash", command="/local/repository/setup-orch-network.sh"))

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
