#!/usr/bin/env python3
"""Parse and plot baseband IQ samples captured via a patched version of srsLTE. """

import argparse
import struct

import matplotlib.pyplot as plt
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument('iqfile', help='IQ file to be parsed/plotted.')
parser.add_argument('--fs', help='Sample rate.', type=float, default=7.68e6)
args = parser.parse_args()

IQ_FILE = args.iqfile
FS = args.fs
IQ_PER_REC = int(FS / 1000)
STRUCT_REP = '8B 3d {}f'.format(IQ_PER_REC * 2)

TS_INDEX      = 8
TX_RATE_INDEX = 9
TX_GAIN_INDEX = 10
IQ_INDEX      = 11


def parse_iq_recs(iq_recs):
    num_iq_recs = len(iq_recs)
    ts = np.zeros((num_iq_recs * IQ_PER_REC))
    tx_rates = np.zeros((num_iq_recs * IQ_PER_REC))
    tx_gains = np.zeros((num_iq_recs * IQ_PER_REC))
    samples = np.zeros((num_iq_recs * IQ_PER_REC), dtype=np.complex)

    for i, iq_rec in enumerate(iq_recs):
        cur_ts = iq_rec[TS_INDEX]
        rec_inds = np.arange(IQ_PER_REC * i, IQ_PER_REC * (i + 1))
        ts[rec_inds] = 1.0 / FS * np.arange(IQ_PER_REC) + cur_ts

        # These don't seem to change, but they may in other cricumstances
        tx_rates[rec_inds] = iq_rec[TX_RATE_INDEX]
        tx_gains[rec_inds] = iq_rec[TX_GAIN_INDEX]

        # data is packed as IQIQIQ... where I and Q are float
        samples[rec_inds] = \
            np.array(iq_rec[IQ_INDEX::2]) + 1j * np.array(iq_rec[IQ_INDEX + 1::2])

    return ts, tx_rates, tx_gains, samples


with open(IQ_FILE, 'rb') as fd:
    barray = fd.read()

iq_struct = struct.Struct(STRUCT_REP)
iq_recs = [iq_struct.unpack(barray[iq_struct.size * i:iq_struct.size * (i + 1)])
           for i in range(len(barray) // iq_struct.size)]
ts, tx_rates, tx_gains, samples = parse_iq_recs(iq_recs)
ts_shifted = ts - ts[0]

plt.figure()
ax1 = plt.subplot(2, 1, 1)
plt.title('In Phase')
plt.plot(ts_shifted, samples.real)
ax2 = plt.subplot(2, 1, 2, sharex=ax1, sharey=ax1)
plt.plot(ts_shifted, samples.imag)
# plt.plot(ts_shifted - 800e-6 - 1 / FS, samples.imag)
plt.title('Quadrature')
plt.xlabel('time (s)')
plt.show()
