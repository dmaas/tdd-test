#!/bin/bash

set -eux
VLANEXISTS=$(ip a | grep -c "eno1.40" 2> /dev/null)
if [ $VLANEXISTS = 0 ]; then
    /local/repository/setup-ue-network.sh
fi

sudo srsue /local/repository/etc/srs-ue.conf
