#!/usr/bin/env bash

# Run appropriate setup script

NODE_ID=$(geni-get client_id)

if [ $NODE_ID = "rue1" ]; then
    /local/repository/setup-ue-network.sh
    /local/repository/setup-ue.sh
elif [ $NODE_ID = "enb1" ]; then
    /local/repository/setup-enb-network.sh
    /local/repository/setup-enb.sh
elif [ $NODE_ID = "epc" ]; then
    /local/repository/setup-epc-network.sh
    /local/repository/setup-epc.sh
else
    echo "no setup necessary"
fi
